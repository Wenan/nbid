#' @import Matrix
#' @import scran
#' @import sva
NULL

#' DE analysis to adjust batch effects

#' This function perform DE analysis adjusting for batch effects. If SVA is
#' used, cell will be sumed into pseudo cells, and latent variables will be
#' inferred and included as covariates. If pseudo bulk method is used, all cells
#' in the same plate (processing unit) will be summed together as pseudo bulk
#' samples. edgeR is used for DE analysis but with different settings for SVA
#' and pseudo bulk based methods.
#'
#' @param count the gene by cell count matrix
#' @param group the group ID for each cell
#' @param method method to use to correct batch effects
#'  Recommended methods: SVA_auto_max20_UMI_scran_B10, this method uses sva to
#'  adjust for batch effects, use at most 20 latent variables, scran is used for
#'  normalization, 10 iteratios are used, 20 cells are summed together to form
#'  a pseudo-cell. Another quick to run and conservative
#'  method is QLedgeR_sum, this is the pseudo bulk method to pool cells into
#'  pseudo bulk samples
#' @param plateID the plate/processing unit ID
#' @param batch used for known matched batches, default is to set the same
#'   value for all cells
#' @param plotPCA to plot PCA+TSNE results
#' @param ncells2sum the number of cells to sum together to form a pseudo cell,
#' default is 20. This is helpful for inference of latent batch effects without
#' much power loss in DE analysis
#' @param imbalanceRatio remove extra cells in a plate if the number of cells
#' is more than imbalanceRatio times of the smallest number of cells among
#' plates. Set it to a very high value, e.g., 1e8, if you don't want to remove
#' any cells. This is used to make sure the number of cells are relatively
#' balanced on each plate, usually a good strategy for correct inference of
#' latent batch effects

#' @return a list of the follow components
#'         result: DE analysis results
#'         timeUsed: the running time
#'         countsForDE: the (pseudo-cell) counts used for DE
#'         groupForDE: the (pseudo-cell) group label used for DE
#'         plateForDE: the (pseudo-cell) plate ID
#'         usedSVDE: the inferred latent variable
#'         pcaPlot: the pca plot
#'         pcaPlotSubsum: the pca plot of pseudo-cells
#'         tsnePlot: the tsne Plot
#'         tsnePlotSubsum: the tsne Plot of pseudo-cells

#' @export
DEAdjustForBatch = function(count, group,
                            method = "SVA_auto_max20_UMI_scran_B10",
                            plateID,
                            batch = rep(1, length(group)),
                            plotPCA = F,
                            ncells2sum = 20,
                            imbalanceRatio = 6) {
  # check data
  if (class(count)[1] != "matrix") {
    count = as.matrix(count)
  }

  all.counts = count
  cell.grouping = group
  plate.of.origin = plateID

  countsForDE = NULL
  groupForDE = NULL
  plateForDE = NULL
  usedSVDE = NULL
  usedSV = NULL

  correctedMatrix = NULL

  subsumCells = F
  pcaPlotSubsum = NULL
  pcaPlot = NULL
  tsnePlotSubsum = NULL
  tsnePlot = NULL

  min.mean = 0.1
  if (regexpr("_scmean[0-9\\.]+", method) != -1) {
    resultRegexpr = regexpr("_scmean[0-9\\.]+", method)
    indexStart = as.numeric(resultRegexpr)
    indexEnd = indexStart + attributes(resultRegexpr)$match.length - 1
    min.mean = as.numeric(substr(method, indexStart + 7, indexEnd))
  }

  if (plotPCA) {
    require(scater)
    logCounts = log(t(t(count) / colSums(count) + 1e-7))
    batchFactor = factor(format(batch, digit = 3))
    sce <- SingleCellExperiment(assays = list(counts = count,
                                              logcounts = logCounts),
                                colData = list(batch = batchFactor,
                                               group = group))
    sce = runPCA(sce)
    pcaPlot = scater::plotPCA(sce, colour_by = "batch",
                              shape_by = "group")
    sce <- runTSNE(sce, dimred="PCA")
    tsnePlot = plotTSNE(sce, colour_by="batch", shape_by = "group")
  }

  tic = proc.time()
  print(method)
  my.env <- new.env()
  my.env$normtype <- "libsize"
  my.env$clusteringFirst = F
  if (regexpr("_scran", method) != -1 ||
      regexpr("_normscran", method) != -1) {
    print("use scran to estimate library size")
    my.env$clusters = plate.of.origin
    my.env$normtype = "deconvolution"
  } else if (regexpr("_cscran", method) != -1) {
    print("use scran to estimate library size with clustering first")
    my.env$clusters = rep(1, length(plate.of.origin))
    my.env$normtype = "deconvolution"
    my.env$clusteringFirst = T
  } else if (regexpr("_nullscran", method) != -1) {
    print("use scran to estimate library size using null genes")
    my.env$clusters = plate.of.origin
    my.env$normtype = "deconvolutionNull"
  } else if (regexpr("_nulllibsize", method) != -1) {
    print("use total library size from null genes")
    my.env$clusters = plate.of.origin
    my.env$normtype = "libSizeNull"
  } else if (regexpr("_ncscran", method) != -1) {
    print("use scran to estimate library size with only 1 cluster")
    my.env$clusters = rep(1, length(plate.of.origin))
    my.env$normtype = "deconvolution"
  } else if (regexpr("_unbiased", method) != -1) {
    my.env$normtype = "unbiased"
  }
  indexNull = 1:dim(count)[1]

  if (method %in% c("edgeR_sum","QLedgeR_sum",
                    "edgeR_sum_batch",
                    "QLedgeR_sum_batch",
                    "edgeR_TMM_sum","QLedgeR_TMM_sum",
                    "edgeR_TMM_sum_batch",
                    "QLedgeR_TMM_sum_batch")){
    my.env$counts <- sumTechReps(all.counts,
                          paste(plate.of.origin, cell.grouping, sep = "_"))
    plate.grouping=unique(cbind(plate.of.origin, group))[, 2]
    if (!is.null(batch) && method %in% c("edgeR_sum_batch",
                                         "QLedgeR_sum_batch",
                                         "edgeR_TMM_sum_batch",
                                         "QLedgeR_TMM_sum_batch") &&
        length(unique(batch)) > 1) {
      batchSum = unique(cbind(plateID, batch))[, 2]
      if (class(batch) == "factor") {
        batchSum = factor(batchSum)
      }
      my.env$sample.data <- data.frame(group=plate.grouping,
                                       batch = batchSum)
      my.env$sample.formula <- ~group + batch
    } else {
      my.env$sample.data <- data.frame(group=plate.grouping)
      my.env$sample.formula <- ~group
    }
    my.env$sample.data <- data.frame(group=plate.grouping)
    my.env$sample.formula <- ~group

    my.env$drop.coefficient <- 2
    if (regexpr("^edgeR", method) != -1) {
      if (regexpr("_TMM", method) != -1) {
        m = "edgeR_TMM"
      } else {
        m = "edgeR"
      }
    } else if (regexpr("^QLedgeR", method) != -1) {
      if (regexpr("_TMM", method) != -1) {
        m = "QLedgeR_TMM"
      } else {
        m = "QLedgeR"
      }
    } else {
      stop("wrong method name")
    }

  } else if(method == "edgeR_batch" || method == "edgeR_batch_scran" ||
            method == "edgeR" || method == "edgeR_scran" ||
            method == "edgeR_batch_ncscran" || method == "edgeR_ncscran" ||
            method == "edgeR_batch_cscran" || method == "edgeR_cscran"){
    my.env$counts = all.counts
    #print(paste("max(|batch|)=", max(abs(batch))))
    #if (max(abs(batch)) > 1e-3) {
    if (!is.null(batch) && !(method %in% c("edgeR", "edgeR_scran",
                                           "edgeR_ncscran", "edgeR_cscran")) &&
        length(unique(batch)) > 1) {
      my.env$sample.data <- data.frame(group=cell.grouping,batch=batch)
      my.env$sample.formula <- ~group+batch
    } else {
      my.env$sample.data <- data.frame(group=cell.grouping)
      my.env$sample.formula <- ~group
    }
    #} else {
    #  print("batch effect too small, not included for stability")
    #  my.env$sample.data <- data.frame(group=cell.grouping)
    #  my.env$sample.formula <- ~group
    #}
    my.env$drop.coefficient <- 2
    m="edgeR"
  } else if (method %in% c("glmer","glmmPQL")){
    my.env$counts = all.counts
    my.env$sample.data <- data.frame(group=cell.grouping)
    my.env$sample.formula <- ~group
    my.env$drop.coefficient <- 2
    my.env$plate.of.origin = plate.of.origin
    m = method

  }else if (method %in% c("RUV_r")){
    my.env2 <- new.env()
    my.env2$counts <- all.counts
    my.env2$sample.data <- data.frame(group=cell.grouping)
    my.env2$sample.formula <- ~group
    my.env2$drop.coefficient <- 2
    my.env2$methods.to.use=c("edgeR")
    sys.source("de_analysis.R", envir=my.env2)
    fit = my.env2$fit
    require(RUVSeq)
    #sig = pvalues$edgeR_sum<0.1
    # str(fit)
    res = residuals(fit,type="deviance")
    # str(res)
    seqRUVr=RUVr(all.counts,!sig,k=20,res)
    my.env$counts = all.counts
    my.env$drop.coefficient <- 2
    my.env$sample.formula <- ~ (group+W_1+W_2+W_3+W_4+W_5+W_6+W_7+W_8+W_9+W_10+W_11+W_12+W_13+W_14+W_15+W_16+W_17+W_18+W_19+W_20)
    my.env$sample.data <- data.frame(seqRUVr$W)
    my.env$sample.data$group = cell.grouping
    m="edgeR"

  } else if (regexpr("^SVA", method) != -1 || regexpr("^SmartSVA", method) != -1 ||
             regexpr("^dSVA", method) != -1 || regexpr("^BCconf", method) != -1 ||
             regexpr("^CorrConf", method) != -1 ||
             regexpr("^zinbwave", method) != -1 ||
             regexpr("^cate", method) != -1 ||
             regexpr("^ComBat", method) != -1 ||
             regexpr("^scMerge", method) != -1 ||
             regexpr("^MNNCorrect", method) != -1
  ){
    n.sv = NULL
    if (regexpr("_k[0-9]+", method) != -1) {
      resultRegexpr = regexpr("_k[0-9]+", method)
      indexStart = as.numeric(resultRegexpr)
      indexEnd = indexStart + attributes(resultRegexpr)$match.length - 1
      n.sv = as.numeric(substr(method, indexStart + 2, indexEnd))
    }

    subsumCells = F
    if ((ncol(all.counts)<1000 && regexpr("sample_cell", method) == -1) ||
        regexpr("_nosum", method) != -1){
      print("no subsum")
      counts.subsum = all.counts
      new.cell.grouping=cell.grouping
      new.plate = plate.of.origin
      new.batch = batch
    } else if (regexpr("nearest_neighbor", method) != -1) {
      # TODO
    } else if (regexpr("sample_cell", method) != -1) {
      counts.subsum = NULL
      new.plate = NULL
      new.cell.grouping = NULL
      indexNNAll = matrix(NA, dim(all.counts)[2], 1)
      indexSampledAll = NULL
      samplePerPlate = 200
      for (p in unique(plate.of.origin)) {
        index = which(plate.of.origin==p)
        indexSampled = sample(index, min(samplePerPlate, length(index)))
        counts = all.counts[, indexSampled]
        counts.subsum = cbind(counts.subsum, counts)
        new.plate = c(new.plate, plate.of.origin[indexSampled])
        new.cell.grouping = c(new.cell.grouping, cell.grouping[indexSampled])

        indexNNAll[indexSampled] = indexSampled
        indexSampledAll = c(indexSampledAll, indexSampled)

        if (length(index) > samplePerPlate) {
          indexNotSampled = setdiff(index, indexSampled)
          indexNNWithinSampled = assignNN(all.counts[, indexSampled, drop = F],
                                  all.counts[, indexNotSampled, drop = F])
          indexNNAll[indexNotSampled] = indexSampled[indexNNWithinSampled]
        }
      }
      print("sampling cells")
      print(table(new.plate, new.cell.grouping))
    } else {
      subsumCells = T
      if (regexpr("-nsum[0-9]+", method) != -1 ||
          regexpr("_nsum[0-9]+", method) != -1) {
        #ncells2sum = as.integer(sub("nsum", "", strsplit(method, "-")[[1]][3]))
        resultRegexpr = regexpr("-nsum[0-9]+|_nsum[0-9]+", method)
        indexStart = as.numeric(resultRegexpr)
        indexEnd = indexStart + attributes(resultRegexpr)$match.length - 1
        ncells2sum = as.numeric(substr(method, indexStart + 5, indexEnd))

        print(paste("number of cells to sum:", ncells2sum))
      }
      counts.ordered = matrix(0,nrow(all.counts),ncol(all.counts))
      plateIDUnique = unique(plate.of.origin)
      for (p in plateIDUnique){
        counts = all.counts[,plate.of.origin==p]
        #counts.ordered[,plate.of.origin==p] = PCAorder(counts)
        if (regexpr("random", method) != -1) {
          counts.ordered[,plate.of.origin==p] =
            counts[, sample(1:dim(counts)[2])]
        } else if (regexpr("UMISim", method) != -1) {
          counts.ordered[,plate.of.origin==p] = UMISimilarOrder(counts,
                                                                ncells2sum)
        } else {
          counts.ordered[,plate.of.origin==p] = UMIOrder(counts, ncells2sum)
        }
      }

      sumResult = subsum(counts.ordered,plate.of.origin,cell.grouping, batch,
                         ncells2sum)
      counts.subsum = sumResult$counts
      new.plate = sumResult$plate.of.origin
      new.cell.grouping = sumResult$cell.grouping
      subsumMapping = sumResult$mapping
      new.batch = factor(sumResult$batch)

      # remove some cells if the cell # is more than imbalanceRatio times of
      # the smallest
      minCells = min(table(new.plate))
      indexKept = NULL
      for (p in unique(new.plate)) {
        if (sum(new.plate == p) > imbalanceRatio * minCells) {
          indexKept = c(indexKept,
                        which(new.plate==p)[1:(imbalanceRatio * minCells)])
        } else {
          indexKept = c(indexKept, which(new.plate==p))
        }
      }
      counts.subsum = counts.subsum[, indexKept]
      new.plate = new.plate[indexKept]
      new.cell.grouping = new.cell.grouping[indexKept]
      new.batch = new.batch[indexKept]
      subsumMapping = subsumMapping[indexKept]
    }

    # form the normalized matrix
    sizeFactor = NULL
    log2FCThreshold = 1
    if (regexpr("_normscran", method) != -1) {
      sizeFactor = getSizeFactor2(counts.subsum, new.plate,
                                  min.mean = min.mean)
    } else {
      sizeFactor = colSums(counts.subsum)
    }
    Y = log(t(t(counts.subsum) / sizeFactor) + 1e-7)

    if (plotPCA & subsumCells) {
      if (is.null(sizeFactor)) {
        sizeFactor = colSums(counts.subsum)
      }
      logCounts = log(t(t(counts.subsum) / sizeFactor) + 1e-7)
      batchFactor = factor(format(new.batch, digit = 3))
      sce <- SingleCellExperiment(assays = list(counts = counts.subsum,
                                                logcounts = logCounts),
                                  colData = list(batch = batchFactor,
                                                 group = new.cell.grouping))
      sce = runPCA(sce)
      pcaPlotSubsum = scater::plotPCA(sce, colour_by = "batch",
                                      shape_by = "group")
      sce <- runTSNE(sce, dimred="PCA")
      tsnePlotSubsum = plotTSNE(sce, colour_by="batch", shape_by = "group")
    }

    if (regexpr("^SVA", method) != -1) {
      iteration = 10
      if (regexpr("-B[0-9]+", method) != -1 ||
          regexpr("_B[0-9]+", method) != -1) {
        #iteration = as.integer(sub("B", "", strsplit(method, "-")[[1]][2]))
        resultRegexpr = regexpr("_B[0-9]+|-B[0-9]+", method)
        indexStart = as.numeric(resultRegexpr)
        indexEnd = indexStart + attributes(resultRegexpr)$match.length - 1
        iteration = as.numeric(substr(method, indexStart + 2, indexEnd))

        print(paste("iteration used:", iteration))
      }
      outlierIteration = 0
      maxOutlierIteration = 0
      indexOutlier = NULL
      while (outlierIteration <= maxOutlierIteration) {
        if (length(indexOutlier) > 0) {
          counts.subsum = counts.subsum[, -indexOutlier]
          new.cell.grouping = new.cell.grouping[-indexOutlier]
          new.plate = new.plate[-indexOutlier]
          print("outliers:")
          print(colnames(counts.subsum)[indexOutlier])
        }
        sizeFactor = NULL
        if (regexpr("_normscran", method) != -1) {
          sizeFactor = getSizeFactor2(counts.subsum, new.plate,
                                      min.mean = min.mean)
        } else {
          sizeFactor = colSums(counts.subsum)
        }
        dat  <- t(t(counts.subsum)/sizeFactor)
        mod  <- model.matrix(~ group, data.frame(group=new.cell.grouping))
        mod0 <- model.matrix(~ 1, data.frame(group=new.cell.grouping))
        require("sva")
        svseq = NULL
        if (regexpr("SVA_auto", method) != -1) {
          svseq <- svaseq(dat, mod, mod0, constant=1e-7, B=iteration)
          if (regexpr("_max[0-9]+", method) != -1) {
            resultRegexpr = regexpr("_max[0-9]+", method)
            indexStart = as.numeric(resultRegexpr)
            indexEnd = indexStart + attributes(resultRegexpr)$match.length - 1
            nMax = as.numeric(substr(method, indexStart + 4, indexEnd))
            if (!is.null(dim(svseq$sv)) && dim(svseq$sv)[2] > nMax) {
              svseq <- svaseq(dat, mod, mod0, n.sv = nMax, constant=1e-7,
                              B=iteration)
            }
          }
          if (is.null(dim(svseq$sv))) {
            dim(svseq$sv) = c(length(svseq$sv), 1)
            colnames(svseq$sv) = "X1"
          }
        } else {
          if (is.null(n.sv)) {
            n.sv = 20
          }
          svseq <- svaseq(dat, mod, mod0, n.sv = n.sv, constant=1e-7,
                          B=iteration)
        }

        indexNull=(svseq$pprob.b < 0.5)
        outlierIteration = outlierIteration + 1
      }

      if (regexpr("sample_cell", method) != -1) {
        svseq$svSampled = svseq$sv
        sv = matrix(NA, dim(all.counts)[2], dim(svseq$sv)[2])
        sv[indexSampledAll, ] = svseq$svSampled
        for (i in 1:dim(all.counts)[2]) {
          if (!(i %in% indexSampledAll)) {
            indexNN = indexNNAll[i]
            sv[i, ] = sv[indexNN, ]
          }
        }
        colnames(sv) = colnames(svseq$svSampled)
        svseq$sv = sv
        counts.subsum = all.counts
        new.cell.grouping=cell.grouping
        new.plate = plate.of.origin
      }

      if (regexpr("SVA_CorrConf", method) != -1) {
        #Y = log(t(t(counts.subsum) / colSums(counts.subsum)) + 1e-7)
        #Y0 = Y[indexNull, ]
        sizeFactor = NULL
        if (regexpr("_normscran", method) != -1) {
          sizeFactor = getSizeFactor2(counts.subsum[indexNull, ], new.plate,
                                      min.mean = min.mean)
        } else {
          sizeFactor = colSums(counts.subsum[indexNull, ])
        }
        Y0 = log(t(t(counts.subsum[indexNull, ]) / sizeFactor + 1e-7))
        X = cbind(1, new.cell.grouping)
        require(CorrConf)
        if (regexpr("_k[0-9]+", method) != -1) {
          resultRegexpr = regexpr("_k[0-9]+", method)
          indexStart = as.numeric(resultRegexpr)
          indexEnd = indexStart + attributes(resultRegexpr)$match.length - 1
          n.sv = as.numeric(substr(method, indexStart + 2, indexEnd))
        } else {
          n.sv = ChooseK(Y0, X)$K.hat
        }
        CorrConfResult = EstimateC_complete(Y0,
                      K = n.sv,X = X[, 2, drop = F], Z = X[, 1, drop = F])
        usedSV = CorrConfResult$C
      } else {
        usedSV = svseq$sv
      }
    } else if (regexpr("^dSVA", method) != -1) {
      require(dSVA)
      # sizeFactor = NULL
      # if (regexpr("_normscran", method) != -1) {
      #   sizeFactor = getSizeFactor2(counts.subsum, new.plate)
      # } else {
      #   sizeFactor = colSums(counts.subsum)
      # }
      #Y = log((t(counts.subsum) / sizeFactor) + 1e-7)
      Y = t(Y)
      X = NULL
      if (regexpr("_platewise", method) != -1) {
        X = as.matrix(new.plate)
      } else {
        X = as.matrix(new.cell.grouping)
      }
      if (!is.null(n.sv)) {
        dSVAResult = dSVA(Y, X, ncomp=n.sv)
      } else {
        if (regexpr("_CBCV", method) != -1) {
          require(CorrConf)
          print("use CBCV to estimate K")
          n.sv = ChooseK(t(Y), cbind(1, X))$K.hat
          dSVAResult = dSVA(Y, X, ncomp=n.sv)
        } else {
          dSVAResult = dSVA(Y, X)
        }
      }
      usedSV = dSVAResult$Z
    } else if (regexpr("^ComBat", method) != -1) {
      # Cannot run if the group ID is confounded with the plate ID
      print("run ComBat")
      if (!is.null(batch) && length(unique(batch)) > 1 &&
          qr(model.matrix(~factor(batch) + factor(group)))$rank >
          qr(model.matrix(~factor(batch)))$rank) {
        dat  <- t(t(counts.subsum)/sizeFactor)
        if (regexpr("^ComBat_log", method) != -1) {
          dat = Y
        }
        mod  <- model.matrix(~ group, data.frame(group=new.cell.grouping))
        mod0 <- model.matrix(~ 1, data.frame(group=new.cell.grouping))
        require("sva")
        if (regexpr("_priorF", method) != -1) {
          par.prior = F
        } else {
          par.prior = T
        }
        try({
          correctedMatrix = ComBat(dat=dat,batch=new.batch, mod=mod,
                                   par.prior=par.prior)
        })
        usedSV = NULL
      } else {
        print("no batch information is available or fully confounding batches")
      }
    } else if (regexpr("^MNNCorrect", method) != -1) {
      if (qr(model.matrix(~factor(batch) + factor(group)))$rank >
          qr(model.matrix(~factor(batch)))$rank) {
        print("run MNNCorrect")
        require(batchelor)
        require(scran)
        sce <- SingleCellExperiment(assays = list(counts = counts.subsum,
                                                  logcounts = Y),
                                    colData = list(batch = new.batch,
                                                cellTypes = new.cell.grouping))
        sceNorm = multiBatchNorm(sce, batch = new.batch)
        dec <- modelGeneVar(sceNorm, design = model.matrix(~new.batch))
        fewer.hvgs = order(dec$bio, decreasing = T)[1:2000]

        #fewer.hvgs = order(rowMeans(Y), decreasing = T)[1:5000]
        #classic.out = mnnCorrect(Y, batch = batch)
        classic.out = mnnCorrect(Y, batch = new.batch, subset.row = fewer.hvgs,
                                 correct.all = T)
        correctedMatrix = classic.out@assays@data@listData[[1]]
      }
    } else if (regexpr("^scMerge", method) != -1) {
      print("scMerge")
      if (qr(model.matrix(~factor(batch) + factor(group)))$rank >
          qr(model.matrix(~factor(batch)))$rank) {
        require(SingleCellExperiment)
        require(scMerge)
        require(scater)
        rownames(counts.subsum) = paste0("row", 1:dim(counts.subsum)[1])
        colnames(counts.subsum) = paste0("col", 1:dim(counts.subsum)[2])
        rownames(Y) = paste0("row", 1:dim(Y)[1])
        colnames(Y) = paste0("col", 1:dim(Y)[2])
        sce <- SingleCellExperiment(assays = list(counts = counts.subsum,
                                                  logcounts = Y),
                                    colData = list(batch = new.batch,
                                            cellTypes = new.cell.grouping))
        # myplot = scater::plotPCA(sce, colour_by = "cellTypes",
        #                          shape_by = "batch")
        exprs_mat = SummarizedExperiment::assay(sce, 'logcounts')[,
                new.plate == names(sort(table(new.plate), decreasing=T))[1]]
        # rownames(exprs_mat) = paste0("row", 1:dim(exprs_mat)[1])
        # colnames(exprs_mat) = paste0("col", 1:dim(exprs_mat)[2])
        segResult = scSEGIndex(exprs_mat = exprs_mat)

        if (regexpr("^scMerge_supervised", method) != -1) {
          scMerge_supervised = scMerge(
            sce_combine = sce,
            ctl = rownames(exprs_mat)[order(segResult$segIdx)[1:2000]],
            kmeansK = rep(2, 3),
            cell_type = sce$cellTypes,
            assay_name = "scMerge_supervised")
          correctedMatrix =
            scMerge_supervised@assays@data$scMerge_supervised
        } else {
          scMerge_unsupervised = scMerge(
            sce_combine = sce,
            ctl = rownames(exprs_mat)[order(segResult$segIdx)[1:2000]],
            kmeansK = rep(2, 3),
            assay_name = "scMerge_unsupervised")
          correctedMatrix =
            scMerge_unsupervised@assays@data$scMerge_unsupervised
        }

      }
    } else if (regexpr("^cate", method) != -1) {
      require(cate)

      # sizeFactor = NULL
      # if (regexpr("_normscran", method) != -1) {
      #   sizeFactor = getSizeFactor2(counts.subsum, new.plate)
      # } else {
      #   sizeFactor = colSums(counts.subsum)
      # }
      #Y = log((t(counts.subsum) / sizeFactor) + 1e-7)
      Y = t(Y)
      X = NULL
      if (regexpr("_platewise", method) != -1) {
        X = as.matrix(new.plate)
      } else {
        X = as.matrix(new.cell.grouping)
      }
      X.data <- data.frame(X = model.matrix(~ X)[, 2])
      if (is.null(n.sv)) {
        if (regexpr("_CBCV", method) != -1) {
          require(CorrConf)
          print("use CBCV to estimate K")
          n.sv = ChooseK(t(Y), model.matrix(~ X))$K.hat
        } else {
          n.sv = est.confounder.num(~ X, X.data, Y = Y, method = "bcv")$r
        }
      }
      # if (n.sv == 0) {
      #   n.sv = 1
      # }
      if (n.sv > 0) {
        cateResult <- cate(~ X, X.data, Y = Y, r = n.sv)
        usedSV = cateResult$Z
      } else {
        usedSV = NULL
      }
    } else if (regexpr("^BCconf", method) != -1 ||
               regexpr("^CorrConf", method) != -1) {
      require(BCconf)
      require(CorrConf)
      usedSV = NULL
      log2FCThreshold = 1
      if (regexpr("_FC0", method) != -1) {
        log2FCThreshold = 0
      }
      if (!is.null(fdr) && !is.null(log2FC) &&
          regexpr("_QLedgeR_sum", method) != -1 ||
          !is.null(fdrTMM) && !is.null(log2FCTMM) &&
          regexpr("_QLedgeR_TMM_sum", method) != -1 ||
          !is.null(fdrCNQ) && !is.null(log2FCCNQ) &&
          regexpr("_CNQ", method) != -1) {
        if (regexpr("_QLedgeR_sum", method) != -1) {
          if (regexpr("_pv0.1", method) != -1) {
            geneUsed = (pvalue > 0.1)
          } else {
            geneUsed = !(fdr < 0.05 & abs(log2FC) > log2FCThreshold)
          }
        } else if (regexpr("_QLedgeR_TMM_sum", method) != -1) {
          if (regexpr("_pv0.1", method) != -1) {
            geneUsed = (pvalueTMM > 0.1)
          } else {
            geneUsed = !(fdrTMM < 0.05 & abs(log2FCTMM) > log2FCThreshold)
          }
        } else if (regexpr("_CNQ", method) != -1) {
          geneUsed = !(fdrCNQ < 0.05 & abs(log2FCCNQ) > log2FCThreshold)
        }
        stopifnot(sum(geneUsed) > 1)
        sizeFactor = NULL
        if (regexpr("_normscran", method) != -1) {
          sizeFactor = getSizeFactor2(counts.subsum[geneUsed, ], new.plate,
                                      min.mean = min.mean)
        } else {
          sizeFactor = colSums(counts.subsum[geneUsed, ])
        }
        Y = log(t(t(counts.subsum[geneUsed, ]) / sizeFactor) + 1e-7)

        if (regexpr("_nullscran", method) != -1) {
          indexNull = geneUsed
        }
      }

      # else {
      #   sizeFactor = NULL
      #   if (regexpr("_normscran", method) != -1) {
      #     sizeFactor = getSizeFactor2(counts.subsum, new.plate)
      #   } else {
      #     sizeFactor = colSums(counts.subsum)
      #   }
      #   Y = log(t(t(counts.subsum) / sizeFactor) + 1e-7)
      # }

      # <debug>
      #source("getSizeFactor.R")
      #sf = getSizeFactor(data = counts.subsum, groups = new.plate)
      #sf = getSizeFactor(data = counts.subsum,
      #                   groups = rep(1, length(new.plate)))
      #Y =  log(t(t(counts.subsum) / sf) + 1e-7)
      # </debug>

      if (regexpr("_platewise", method) != -1) {
        X = cbind(1, new.plate)
      } else {
        X = cbind(1, new.cell.grouping)
      }
      if (is.null(n.sv)) {
        if (regexpr("_CBCV", method) != -1 ||
            regexpr("^CorrConf", method) != -1) {
          print("use CBCV to estimate K")
          n.sv = ChooseK(Y, X)$K.hat
        } else if (regexpr("_BCV", method) != -1) {
          print("use BCV to estimate K")
          averageCount = rowMeans(counts.subsum)
          if (regexpr("_allGenes", method) != -1) {
            n.sv = est.conf.num(Y, X, max.r = 20)$r
          } else {
            index = order(averageCount, decreasing = T)[
              1:min(length(averageCount), 2000)]
            n.sv = est.conf.num(Y[index, ], X, max.r = 20)$r
          }
        } else {
          print("use eigen decomposition to estimate K")
          n.sv = est.conf.num(Y, X, max.r = 20, method = "ed")
        }
      }

      if (regexpr("_p[0-9]+", method) != -1) {
        resultRegexpr = regexpr("_p[0-9]+", method)
        indexStart = as.numeric(resultRegexpr)
        indexEnd = indexStart + attributes(resultRegexpr)$match.length - 1
        n.sv.added = as.numeric(substr(method, indexStart + 2, indexEnd))
        n.sv = n.sv + n.sv.added
      }

      # <debug>
      # if (n.sv == 0) {
      #   n.sv = 1
      # }
      # </debug>

      if (regexpr("^BCconf", method) != -1) {
        BCconfResult = Correction(Y, X, ind.cov = 2, r.confound = n.sv)
        usedSV = BCconfResult$C
      } else if (regexpr("^CorrConf", method) != -1) {
        if (n.sv > 0) {
          CorrConfResult = EstimateC_complete(Y, K = n.sv,
                                  X = X[, 2, drop = F], Z = X[, 1, drop = F])
          usedSV = CorrConfResult$C
        } else {
          usedSV = NULL
        }
      }
    } else if (regexpr("^zinbwave", method) != -1) {
      print("run zinbwave")

      if (is.null(n.sv)) {
        require(BCconf)
        require(CorrConf)
        usedSV = NULL
        # Y = log(t(t(counts.subsum) / colSums(counts.subsum)) + 1e-7)
        if (regexpr("_platewise", method) != -1) {
          X = cbind(1, new.plate)
        } else {
          X = cbind(1, new.cell.grouping)
        }
        if (regexpr("_CBCV", method) != -1) {
          print("use CBCV to estimate K")
          n.sv = ChooseK(Y, X)$K.hat
        } else if (regexpr("_BCV", method) != -1) {
          print("use BCV to estimate K")
          averageCount = rowMeans(counts.subsum)
          index = order(averageCount, decreasing = T)[1:2000]
          n.sv = est.conf.num(Y[index, ], X, max.r = 20)$r
        }
      }
      nGenes = NULL
      if (regexpr("_g[0-9]+", method) != -1) {
        resultRegexpr = regexpr("_g[0-9]+", method)
        indexStart = as.numeric(resultRegexpr)
        indexEnd = indexStart + attributes(resultRegexpr)$match.length - 1
        nGenes = as.numeric(substr(method, indexStart + 2, indexEnd))
      }
      covariates = NULL
      if (regexpr("_cov", method) != -1) {
        covariates = model.matrix(~new.cell.grouping)
      }
      # if (regexpr("^zinbwave_offset", method) != -1) {
      #   usedSV = runZinbwave(counts.subsum,
      #                        covariates = as.matrix(new.cell.grouping),
      #                        offset = T, K = n.sv, nGenes = nGenes)
      # } else if (regexpr("^zinbwave", method) != -1) {
      #   usedSV = runZinbwave(counts.subsum,
      #                        covariates = as.matrix(new.cell.grouping),
      #                        offset = F, K = n.sv, nGenes = nGenes)
      # }
      # no zero inflation
      zinbResult = runZinbwave(counts.subsum, sizeFactor = sizeFactor,
                               covariates = covariates,
                               K = n.sv, nGenes = nGenes, ncore = 1,
                               zeroinflation = F)
      objectDataZinb = zinbResult$objectDataZinb
      geneSubset = zinbResult$subset
      W <- reducedDim(objectDataZinb)
      if (regexpr("_normalize", method) != -1) {
        correctedMatrixSubset =
          objectDataZinb@assays@data@listData$normalizedValues
        correctedMatrix = matrix(0, dim(counts.subsum)[1],
                                 dim(counts.subsum)[2])
        correctedMatrix[geneSubset, ] = correctedMatrixSubset
      } else {
        usedSV = W
      }
    }

    countsForDE = NULL
    groupForDE = NULL
    usedSVDE = NULL
    plateForDE = NULL
    if (regexpr("_mapback", method) != -1 && subsumCells) {
      # assume counts.ordered is generated
      resultMapback = mapback(counts.ordered, subsumMapping,
                              new.plate, new.cell.grouping, usedSV)
      countsForDE = resultMapback$countsMapped
      groupForDE = resultMapback$groupMapped
      usedSVDE = resultMapback$SVMapped
      plateForDE = resultMapback$plateMapped
    } else {
      countsForDE = counts.subsum
      groupForDE = new.cell.grouping
      usedSVDE = usedSV
      plateForDE = new.plate

      if (is.null(new.plate)) {
        plateForDE = rep(1, length(groupForDE))
      }
    }

    if (regexpr("_scran", method) != -1 ||
        regexpr("_normscran", method) != -1) {
      my.env$clusters = plateForDE
    } else if (regexpr("_cscran", method) != -1) {
      my.env$clusters = plateForDE
    } else if (regexpr("_ncscran", method) != -1) {
      my.env$clusters = rep(1, length(plateForDE))
    } else if (regexpr("_nullscran", method) != -1 ||
               regexpr("_nulllibsize", method) != -1) {
      my.env$clusters = plateForDE
      my.env$indexNull = indexNull
    }

    if (regexpr("_adjust", method) != -1) {
      R2 = summary(lm(as.numeric(groupForDE) ~ usedSVDE))$r.squared
      if (R2 > 0.5) {
        source("adjustLV.R")
        usedSVDE = adjustLV(usedSVDE, groupForDE)
      }
    }

    # round the count matrix for edgeR
    countsForDE = round(countsForDE)
    my.env$counts = countsForDE
    nSV = dim(usedSVDE)[2]
    if (length(nSV) > 0) {
      print(paste("nSV=", nSV))
      colnames(usedSVDE) = paste0("X", 1:nSV)
      my.env$sample.formula = as.formula(paste("~(group",
                        paste(paste0("+X", 1:nSV), collapse = ""), ")"))
      sample.data = data.frame(usedSVDE)
      sample.data$group = as.matrix(groupForDE, ncol = 1)
    } else {
      print(paste("nSV=0"))
      my.env$sample.formula = as.formula("~group")
      sample.data = data.frame(group = groupForDE)
    }
    my.env$drop.coefficient <- 2
    my.env$sample.data <- sample.data
    m = "edgeR"
  } else{
    stop("test method not recognized", call.=FALSE)
  }

  if (regexpr("^ComBat", method) != -1 ||
      regexpr("^scMerge", method) != -1 ||
      regexpr("^MNNCorrect", method) != -1 ||
      (regexpr("^zinbwave", method) != -1 &&
       regexpr("_normalize", method) != -1)) {
    mod  <- model.matrix(~ group, data.frame(group=new.cell.grouping))
    mod0 <- model.matrix(~ 1, data.frame(group=new.cell.grouping))
    require("sva")

    # use sva
    if (!is.null(correctedMatrix)) {
      pv = f.pvalue(correctedMatrix,mod,mod0)
    } else {
      pv = NA
    }
  } else {
    my.env$methods.to.use = c(m)
    my.env$min.mean = min.mean
    resultDE = runDE(my.env$counts, my.env$sample.formula, my.env$sample.data,
                     my.env$drop.coefficient,
                     my.env$methods.to.use,
                     my.env$normtype,
                     my.env$clusters,
                     my.env$clusteringFirst,
                     my.env$min.mean)
    if (m %in% c("edgeR", "edgeR_TMM")) {
      pv = resultDE[["edgeR"]]
    } else if (m %in% c("QLedgeR", "QLedgeR_TMM")) {
      pv = resultDE[["QLedgeR"]]
    }
  }

  toc = proc.time()
  timeUsed = toc - tic

  countStat = T
  result = result.summary(all.counts,plate.of.origin,pv,is.de = NULL,
                          countStat)
  if (m %in% c("edgeR", "edgeR_TMM")) {
    result$logFC=resultDE$res$table$logFC
  }else if (m %in% c("QLedgeR", "QLedgeR_TMM")) {
    result$logFC=resultDE$qres$table$logFC
  } else {
    result$logFC = NA
  }

  return(list(result = result,
              timeUsed = timeUsed,
              countsForDE = countsForDE,
              groupForDE = groupForDE,
              plateForDE = plateForDE,
              usedSVDE = usedSVDE,
              pcaPlot = pcaPlot,
              pcaPlotSubsum = pcaPlotSubsum,
              tsnePlot = tsnePlot,
              tsnePlotSubsum = tsnePlotSubsum))
}
