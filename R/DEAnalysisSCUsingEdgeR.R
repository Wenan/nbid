#' @import edgeR
#' @import scran
NULL

#' DE analysis of scRNA-seq data using edgeR with specific parameters tuned
#' for UMI count based single cell data
#'
#' edgeR is developed for bulk RNA-seq using the negative binomial model.
#' However, scRNA-seq with UMI counts can also be modeled using the negative
#' binomial model. With specific parameter values, edgeR can be used for DE
#' analysis of scRNA-seq data
#'
#' @param count the vector of gene counts
#' @param groups the vector of group information
#' @param covariates the covariates
#' @param scran if true, scran is used for normalization
#' @param clusteringFirstInScran if true, clustering is performed in scran
#'   before normalization
#' @param cluster the cluster information provided to scran for clustering. If
#'   NULL, then scran will do clustering to infer clusters by itself
#' @param min.row.sum the parameter passed to estimateDisp in edgeR, used to
#'        filter genes by specifying minimal total counts across samples

#' @return a data frame of the test results and related statistics

#' @export
DEAnalysisSCUsingEdgeR = function(count, group, covariates = NULL,
                                  scran = T, clusteringFirstInScran = T,
                                  cluster = NULL,
                                  min.row.sum = 1) {

  sizeFactor = NULL
  if (scran) {
    if (is.null(cluster)) {
      cluster = rep(1, dim(count)[2])
    }
    sizeFactor = getSizeFactor2(count, cluster,
                                clusteringFirst = clusteringFirstInScran,
                                clusteringThreshold = Inf,
                                min.mean = 0.1)
  }

  # make the rownames unique
  rownames(count) = paste0(rownames(count), "_row", 1:dim(count)[1])

  # use edgeR
  DEResult = DEUsingEdgeR(data = count,
                          groups = group,
                          sizeFactor = sizeFactor,
                          covariates = covariates,
                          useTMM = F,
                          log2FoldChangeThreshold = 0,
                          useLRT = T,
                          dispersionEstimate = 0,
                          span = NULL,
                          prior.count = 0,
                          min.row.sum = min.row.sum)
  countStatistics = calculateCountStatistics(data = count,
                                             group = group,
                                             nameOrder = rownames(DEResult))
  geneNames = rownames(DEResult)
  output = cbind(geneNames, DEResult, countStatistics$statTable)


  return(output)
}
