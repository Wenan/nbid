# Negative binomial with independent dispersions (NBID) for UMI based scRNA-seq data and other DE analysis functions #

### Update 4/14/2021 ###
I added a wrapper function DEAnalysisSCUsingEdgeR to use edgeR with specific parameters tuned for DE analysis of scRNA-seq data, this assumes a common dispersion among different groups. To adjust for batch effects, I added the function DEAdjustForBatch, the recommended method uses SVA to infer the latent batch variables. The pseudo bulk method is quick, robust but potentially with lower power, good for identifying large effects.

### What is this repository for? ###

This package provides functions for modeling UMI based single cell RNA-seq data and differential expression analysis using negative binomial with independent dispersions.

### How do I get set up? ###

* Summary of set up, make sure the latest pandoc is installed for vignettes build 
```R
library(devtools)
install_bitbucket("Wenan/nbid", ref="master", build_vignettes = T)
```
* use the package
```R
library(NBID)
help(package = "NBID")
```

### Tutorial ###
```R
vignette("NBID")
```

### Who do I talk to? ###

* Please contact Wenan Chen (wenan.chen@stjude.org) for any questions

### License ###
MIT license
